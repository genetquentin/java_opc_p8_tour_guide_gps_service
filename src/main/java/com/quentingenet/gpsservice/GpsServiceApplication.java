package com.quentingenet.gpsservice;

import gpsUtil.GpsUtil;
import org.apache.tomcat.jni.Local;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Locale;

@SpringBootApplication
public class GpsServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(GpsServiceApplication.class, args);
  }

  @Bean
  public GpsUtil getGpsUtil() {
    Locale.setDefault(Locale.US);
    return new GpsUtil();
  }

}
