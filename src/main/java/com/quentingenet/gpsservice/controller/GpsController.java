package com.quentingenet.gpsservice.controller;

import com.quentingenet.gpsservice.service.GpsService;
import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
public class GpsController {

  private final Logger logger = LoggerFactory.getLogger(GpsController.class);

  @Autowired
  private GpsService gpsService;

  @GetMapping("/attractions")
  public List<Attraction> getAttractions() {
    logger.info("GET /attractions");
    return gpsService.getAttractions();
  }

  @GetMapping("/locations")
  public VisitedLocation getUserLocation(@RequestParam("userId") UUID userId) {
    logger.info("GET /locations?userId={}", userId);
    return gpsService.getUserLocation(userId);
  }

}
