package com.quentingenet.gpsservice.service;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class GpsService {

  @Autowired
  private GpsUtil gpsUtil;

  public List<Attraction> getAttractions() {
    return gpsUtil.getAttractions().stream().map(attraction ->
        new Attraction(
            attraction.attractionName,
            attraction.city,
            attraction.state,
            attraction.latitude,
            attraction.longitude
        )
    ).collect(Collectors.toList());
  }

  public VisitedLocation getUserLocation(UUID userId) {
    VisitedLocation oldVisitedLocation = gpsUtil.getUserLocation(userId);
    return new VisitedLocation(userId, new Location(
        oldVisitedLocation.location.latitude,
        oldVisitedLocation.location.longitude),
        oldVisitedLocation.timeVisited);
  }

}
