FROM openjdk:8-alpine
COPY build/libs/GpsService-0.0.1-SNAPSHOT.jar gps-service.jar
CMD ["java", "-jar", "gps-service.jar"]
